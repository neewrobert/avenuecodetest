package shop;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.avenuecode.shop.resource.ProductRestController;

@RunWith(SpringRunner.class)
@ComponentScan("com.avenuecode.shop")
@EntityScan("com.avenuecode.shop.model")
@EnableJpaRepositories("com.avenuecode.shop.dao")
@TestPropertySource(locations="classpath:test.properties")
@SpringBootTest(classes = ProductRestController.class)
public class ApplicationTests {
	
	@Test
	public void contextLoads() {
		
	}

}
