package shop;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.avenuecode.shop.model.ProductModel;
import com.avenuecode.shop.resource.PictureRestController;
import com.avenuecode.shop.resource.ProductRestController;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ComponentScan("com.avenuecode.shop")
public class ProductRestControllerTest extends ApplicationTests {

	private MockMvc mockMvc;

	@Autowired
	private ProductRestController productRestController;

	public final String REST_SERVICE_URI = "http://localhost:8080/api";

	private ProductModel pictureMock;

	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(productRestController).build();
	}

	/**
	 * This Test Case, tests if the connection with are ok
	 * 
	 * @throws Exception
	 */
	@Test
	public void test1GetIndexProductRestController() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get(REST_SERVICE_URI + "/product/"))
				.andExpect(MockMvcResultMatchers.status().isNoContent());
	}

	@Test
	public void test2CreateProducts() throws Exception {

		pictureMock = new ProductModel();
		pictureMock.setName("laptop");
		pictureMock.setDescription("laptop dell 15 inc");

		ProductModel productMouse = new ProductModel();
		productMouse.setName("mouse");
		productMouse.setDescription("mouse logitech");

		List<ProductModel> childProducts = new ArrayList<ProductModel>();
		childProducts.add(productMouse);
		pictureMock.setChildProducts(childProducts);

		String productJson = pictureMock.toJson();

		this.mockMvc
				.perform(MockMvcRequestBuilders.post(REST_SERVICE_URI + "/product/")
						.contentType(MediaType.APPLICATION_JSON).content(productJson))
				.andExpect(MockMvcResultMatchers.status().isCreated());

	}

	@Test
	public void test3GetProduct() throws Exception {

		this.mockMvc.perform(MockMvcRequestBuilders.get(REST_SERVICE_URI + "/product/1"))
				.andExpect(MockMvcResultMatchers.status().isOk());

	}

	@Test
	public void test4UpdateProduct() throws Exception {

		ProductModel productLaptop = new ProductModel();
		productLaptop.setName("laptop update");
		productLaptop.setDescription("laptop dell 15 inc update");

		ProductModel productMouse = new ProductModel();
		productMouse.setName("mouse update");
		productMouse.setDescription("mouse logitech update");

		List<ProductModel> childProducts = new ArrayList<ProductModel>();
		childProducts.add(productMouse);
		productLaptop.setChildProducts(childProducts);

		String productJson = productLaptop.toJson();

		this.mockMvc
				.perform(MockMvcRequestBuilders.put(REST_SERVICE_URI + "/product/1")
						.contentType(MediaType.APPLICATION_JSON).content(productJson))
				.andExpect(MockMvcResultMatchers.status().isOk());

	}

	@Test
	public void test5DeleteProduct() throws Exception {

		this.mockMvc.perform(MockMvcRequestBuilders.delete(REST_SERVICE_URI + "/product/1"))
				.andExpect(MockMvcResultMatchers.status().isNoContent());

	}

}
