package com.avenuecode.shop.model;

public abstract class GenericModel {
	
	private long id;
	
	public abstract String toXML();
	public abstract String toJson();

}
