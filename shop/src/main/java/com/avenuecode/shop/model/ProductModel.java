package com.avenuecode.shop.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.thoughtworks.xstream.XStream;

@Entity
@Table(name = "product")
public class ProductModel extends GenericModel implements Serializable {

	/**
	 * @author Newton Roberto dos Santos
	 * 
	 *         This class is a model of a product
	 * 
	 */
	private static final long serialVersionUID = 6018798950854087477L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Expose
	private String name;
	
	@Expose
	private String description;
	
	@Expose
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<ProductModel> childProducts;

	@Expose
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<PictureModel> pictures;

	/**
	 * Default constructor
	 */
	public ProductModel() {

	}

	public ProductModel(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	public List<PictureModel> getPictures() {
		return pictures;
	}

	public void setPictures(List<PictureModel> pictures) {
		this.pictures = pictures;
	}

	public ProductModel(int id, String name, String description, List<ProductModel> childProducts,
			List<byte[]> pictures) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.childProducts = childProducts;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<ProductModel> getChildProducts() {
		return childProducts;
	}

	public void setChildProducts(List<ProductModel> childProducts) {
		this.childProducts = childProducts;
	}

	@Override
	public String toXML() {
		return new XStream().toXML(this);
	}

	@Override
	public String toJson() {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
		return gson.toJson(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductModel other = (ProductModel) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProductModel [name=" + name + ", description=" + description + "]";
	}

}
