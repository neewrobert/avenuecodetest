package com.avenuecode.shop.resource;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.avenuecode.shop.dao.ProductDao;
import com.avenuecode.shop.model.ProductModel;
import com.avenuecode.shop.util.CustomErrorType;

@SpringBootApplication
@RestController
@RequestMapping("/api")
public class ProductRestController {

	public static final Logger logger = LoggerFactory.getLogger(ProductRestController.class);

	@Autowired
	private ProductDao productDao;

	/**
	 * Create a Product
	 * 
	 * @param product
	 * @param ucBuilder
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/product/", method = RequestMethod.POST)
	public ResponseEntity<?> createProduct(@RequestBody ProductModel product) {

		logger.info("Creating product : {}", product);

		if (productDao.isProductExist(product)) {
			logger.error("Unable to create. A Product with name {} already exist", product.getName());
			return new ResponseEntity(
					new CustomErrorType(
							"Unable to create. A Product with name " + product.getName() + " already exist."),
					HttpStatus.CONFLICT);
		}

		productDao.insert(product);
		logger.info("The product with name {} was creted ans saved successfully", product.getName());
		return new ResponseEntity<String>(HttpStatus.CREATED);
	}

	/**
	 * Update a product
	 * 
	 * @param id
	 * @param product
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/product/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateProduct(@PathVariable("id") long id, @RequestBody ProductModel product) {

		ProductModel currentProduct = productDao.findById(id);

		if (currentProduct == null) {
			logger.error("Unable to update. Product with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to upate. Product with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}

		currentProduct.setName(product.getName());
		currentProduct.setDescription(product.getDescription());
		currentProduct.setChildProducts(product.getChildProducts());
		currentProduct.setPictures(product.getPictures());

		productDao.update(currentProduct);
		logger.info("The Product with name {} has been successfully updated and saved", currentProduct.getName());
		return new ResponseEntity<ProductModel>(currentProduct, HttpStatus.OK);
	}

	/**
	 * Retrieve Single Product without relationship (pictures and products
	 * childs)
	 * 
	 * @param id
	 *            of a product
	 * @return a ResponseEntity (json) of the product
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getSimpleProduct(@PathVariable("id") long id) {

		logger.info("Fetching User with id {}", id);
		ProductModel product = productDao.findById(id);

		if (product == null) {
			logger.error("User with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Product with id " + id + " not found"),
					HttpStatus.NOT_FOUND);
		}

		ProductModel simpleProduct = new ProductModel();
		simpleProduct.setId(product.getId());
		simpleProduct.setName(product.getName());
		simpleProduct.setDescription(product.getDescription());
		logger.info("Returning a simple product with name {}", simpleProduct.getName());
		return new ResponseEntity<ProductModel>(simpleProduct, HttpStatus.OK);
	}

	/**
	 * Retrieve Single Product and its childs (pictures and products childs)
	 * 
	 * @param id
	 *            of a product
	 * @return a ResponseEntity (json) of the product
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/product/{id}/complete", method = RequestMethod.GET)
	public ResponseEntity<?> getProductComplete(@PathVariable("id") long id) {

		logger.info("Fetching Product with id {}", id);
		ProductModel product = productDao.findById(id);

		if (product == null) {
			logger.error("Product with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Product with id " + id + " not found"),
					HttpStatus.BAD_REQUEST);
		}
		
		logger.info("Returning a complete product with name {}", product.getName());
		return new ResponseEntity<ProductModel>(product, HttpStatus.OK);
	}

	/**
	 * Retrieve all children of a specific product (pictures and products childs)
	 * 
	 * @param id
	 *            of a product
	 * @return a ResponseEntity (json) of the product
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/product/{id}/complete-childs", method = RequestMethod.GET)
	public ResponseEntity<?> getProductChildsComplete(@PathVariable("id") long id) {

		logger.info("Fetching Product with id {}", id);
		ProductModel product = productDao.findById(id);

		if (product == null) {
			logger.error("Product with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Product with id " + id + " not found"),
					HttpStatus.BAD_REQUEST);
		}
		
		logger.info("Returning a complete product with name {}", product.getName());
		
		List<ProductModel> childrenProducts = product.getChildProducts();
		
		for (ProductModel productModel : childrenProducts) {
			productModel.setChildProducts(null);
		}
		
		return new ResponseEntity<List<ProductModel>>(childrenProducts, HttpStatus.OK);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/product/{id}/simple-childs", method = RequestMethod.GET)
	public ResponseEntity<?> getProductChildsShort(@PathVariable("id") long id) {

		logger.info("Fetching Product with id {}", id);
		ProductModel product = productDao.findById(id);
		
		if (product == null) {
			logger.error("Product with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Product with id " + id + " not found"),
					HttpStatus.BAD_REQUEST);
		}
		
		logger.info("Returning a complete product with name {}", product.getName());
		return new ResponseEntity<List<ProductModel>>(product.getChildProducts(), HttpStatus.OK);
	}
	
	/**
	 * Delete a product
	 * 
	 * @param id
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteProduct(@PathVariable("id") long id) {
		logger.info("Fetching & Deleting Product with id {}", id);

		ProductModel product = productDao.findById(id);
		if (product == null) {
			logger.error("Unable to delete. Product with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to delete. Product with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}
		productDao.remove(product);
		logger.info("The product has been successfully removed");
		return new ResponseEntity<ProductModel>(HttpStatus.NO_CONTENT);
	}
	
	/**
	 * Get all products
	 * @return a list of all products available
	 */
	@RequestMapping(value = "/product/", method = RequestMethod.GET)
	public ResponseEntity<List<ProductModel>> listAllProducts() {
		List<ProductModel> products = productDao.getAll();
		if (products.isEmpty()) {
			logger.info("No products were found");
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		
		logger.info("Returning {} products", products.size());
		return new ResponseEntity<List<ProductModel>>(products, HttpStatus.OK);
	}

}
