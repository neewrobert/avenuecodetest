package com.avenuecode.shop.resource;

import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import com.avenuecode.shop.dao.PictureDao;
import com.avenuecode.shop.model.PictureModel;
import com.avenuecode.shop.util.CustomErrorType;

@RestController
@RequestMapping("/api")
public class PictureRestController {
	public static final Logger logger = LoggerFactory.getLogger(PictureRestController.class);

	@Autowired
	private PictureDao pictureDao;

	/**
	 * Create a Picture
	 * 
	 * @param picture
	 * @param ucBuilder
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/picture/", method = RequestMethod.POST)
	public ResponseEntity<?> createPicture( @RequestBody PictureModel picture, UriComponentsBuilder ucBuilder,
			@RequestParam("file") MultipartFile file) {
		logger.info("Creating picture : {}", picture);

		if (pictureDao.isPictureExist(picture)) {
			logger.error("Unable to create. A Picture with name {} already exist", picture.getName());
			return new ResponseEntity(
					new CustomErrorType(
							"Unable to create. A Picture with name " + picture.getName() + " already exist."),
					HttpStatus.CONFLICT);
		}
		if (!file.isEmpty()) {
			try {
				picture.setName(picture.getName() == null ? file.getName() : picture.getName());
				picture.setType(file.getContentType());
				picture.setBytes(Base64.getEncoder().encode(file.getBytes()));
			} catch (Exception e) {
				logger.error(e.getMessage());
				return new ResponseEntity(new CustomErrorType("Unable to upload picture"),
						HttpStatus.NOT_FOUND);
			}
		}

		pictureDao.insert(picture);
		logger.info("Picture with name {} has been created", picture.getName());
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/api/picture/{id}").buildAndExpand(picture.getId()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}


	/**
	 * Update a picture
	 * 
	 * @param id
	 * @param picture
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/picture/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updatePicture(@PathVariable("id") long id, @RequestBody PictureModel picture, @RequestParam("file") MultipartFile file) {

		PictureModel currentPicture = pictureDao.findByName(file.getName());

		if (currentPicture == null) {
			logger.error("Unable to update. Picture with name {} not found.", file.getName());
			return new ResponseEntity(new CustomErrorType("Unable to upate. Picture with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}
		
		if (!file.isEmpty()) {
			try {
				picture.setName(file.getName());
				picture.setType(file.getContentType());
				picture.setBytes(Base64.getEncoder().encode(file.getBytes()));
			} catch (Exception e) {
				logger.error(e.getMessage());
				return new ResponseEntity(new CustomErrorType("Unable to upload picture"),
						HttpStatus.BAD_REQUEST);
			}
		}
		
		picture.setId(currentPicture.getId());
		logger.info("Picture with name {} has been updated", picture.getName());
		pictureDao.update(picture);
		return new ResponseEntity<PictureModel>(currentPicture, HttpStatus.OK);
	}

	/**
	 * 
	 * @param id of a picture of the image to be returned
	 * @return a ResponseEntity (json) of the picture with/or httpstatus of the request
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/picture/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getSimplePicture(@PathVariable("id") long id) {

		logger.info("Fetching Picture with id {}", id);
		PictureModel picture = pictureDao.findById(id);

		if (picture == null) {
			logger.error("User with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Picture with id " + id + " not found"),
					HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<PictureModel>(picture, HttpStatus.OK);
	}


	/**
	 * Delete a picture
	 * RequestMethod.DELETE
	 * @param id of a picture to be deleted
	 * @return a HttpStatus about the success of a request
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/picture/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deletePicture(@PathVariable("id") long id) {
		logger.info("Fetching & Deleting Picture with id {}", id);

		PictureModel picture = pictureDao.findById(id);
		if (picture == null) {
			logger.error("Unable to delete. Picture with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to delete. Picture with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}
		pictureDao.remove(picture);
		return new ResponseEntity<PictureModel>(HttpStatus.NO_CONTENT);
	}

}
