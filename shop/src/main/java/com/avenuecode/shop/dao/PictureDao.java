package com.avenuecode.shop.dao;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.avenuecode.shop.model.PictureModel;
import com.avenuecode.shop.model.ProductModel;

@Service("pictureDao")
@Transactional
@Repository
public class PictureDao implements Serializable {

	private static final long serialVersionUID = 2519881187837576543L;

	@PersistenceContext
	EntityManager em;

	private GenericDAO<PictureModel> dao;

	@PostConstruct
	public void init() {
		this.dao = new GenericDAO<>(this.em, PictureModel.class);
	}

	public void insert(PictureModel t) {
		dao.insert(t);
	}

	public void remove(PictureModel t) {
		dao.remove(t);
	}

	public void update(PictureModel t) {
		dao.update(t);
	}

	public List<PictureModel> getAll() {
		return dao.getAll();
	}

	public PictureModel findById(long id) {
		return dao.findById(id);
	}

	public boolean isPictureExist(PictureModel picture) {

		List<PictureModel> pictures = dao.getAll();
		if (pictures.isEmpty()) {
			return false;
		}
		for (PictureModel pictureModel : pictures) {
			if (pictureModel.equals(picture)) {
				return true;
			}
		}

		return false;
	}

	public PictureModel findByName(String name) {
		return dao.findByName(name);
	}

}
