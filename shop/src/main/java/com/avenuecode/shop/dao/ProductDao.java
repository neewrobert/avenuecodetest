package com.avenuecode.shop.dao;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.avenuecode.shop.model.ProductModel;

@Service("productDao")
@Transactional
@Repository
public class ProductDao implements Serializable {
	
	private static final long serialVersionUID = -4831787811363721661L;
	
	@PersistenceContext
	EntityManager em;
	
	private GenericDAO<ProductModel> dao;
	
	@PostConstruct
	public void init(){
		this.dao = new GenericDAO<>(this.em, ProductModel.class);
	}
	
	public void insert(ProductModel t) {
		dao.insert(t);
	}
	
	public void remove(ProductModel t) {
		dao.remove(t);
	}
	
	public void update(ProductModel t) {
		dao.update(t);
	}
	
	public List<ProductModel> getAll() {
		return dao.getAll();
	}
	
	public ProductModel findById(long id) {
		return dao.findById(id);
	}
	
	public boolean isProductExist(ProductModel product) {
		
		List<ProductModel> products = dao.getAll();
		if(products.isEmpty()){
			return false;
		}
		for (ProductModel productModel : products) {
			if(productModel.equals(product)){
				return true;
			}
		}
		
		return false;
	}
	

}
