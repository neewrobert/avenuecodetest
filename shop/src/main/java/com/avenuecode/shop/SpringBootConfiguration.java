package com.avenuecode.shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"com.avenuecode.shop"})
public class SpringBootConfiguration {
	
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootConfiguration.class, args);
	}

}
