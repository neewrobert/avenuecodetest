
To install project e get .jar file, use the command mvn: clean install package
To test the project, use the commmand mvn: test
To run the project use the command mvn: sping-boot:run

all the request uses a Json file

To create a Product: [POST] http://localhost:8080/avenueShopApi/api/product/
To update a Product: [PUT] http://localhost:8080/avenueShopApi/api/product/{id}
To delete a Product: [DELETE] http://localhost:8080/avenueShopApi/api/product/{id}
To get a product with no relationship: [GET] http://localhost:8080/avenueShopApi/api/product/{id}
To get a Product with relationship: [GET] http://localhost:8080/avenueShopApi/api/product/{id}}/complete
To get all children of a specific product with its relationships: [GET] http://localhost:8080/avenueShopApi/api/product/{id}}/complete-childs
To get all children of a specific product without relationships: [GET] http://localhost:8080/avenueShopApi/api/product/{id}}/simple-childs


To create a picture: [POST] http://localhost:8080/avenueShopApi/api/picture/
To update an image:  [PUT] http://localhost:8080/avenueShopApi/api/picture/{id}
To delete an image: [DELETE] http://localhost:8080/avenueShopApi/api/picture/{id}
To get an image: [GET] http://localhost:8080/avenueShopApi/api/picture/{id} Obs: Return a Base64 in the JSON